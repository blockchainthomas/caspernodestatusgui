#!/bin/bash
echo "Casper Node Status GUI setup"
if [ -f config_default.json ]
then
	CONFIG=$(cat config_default.json)
	DETECTED_IP=$(curl -s ifconfig.co)
	read -p "What is your node's IP address (detected $DETECTED_IP)> " MYIP
	if [ -z "$MYIP" ]; then
		MYIP=$DETECTED_IP
	fi
	[[ $MYIP =~ ^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$ ]] && RGX=true || RGX=false
	if [ $RGX = true ]; then
		read -p "Port to host the server (default 8080)> " MYPORT
		if [ -z "$MYPORT" ]; then
			MYPORT=8080
		fi
		if [ $MYPORT -lt 49151 ] && [ $MYPORT -gt 1024 ] && [[ $MYPORT =~ ^[0-9]+$ ]]; then
			read -p "Node json logs path (optional, default ~/.casperlabs/logs/log.0.txt)> " LOGSPATH
			if [ -z "$LOGSPATH" ]; then
				LOGSPATH="/home/$USER/.casperlabs/logs/log.0.txt"
			fi
			read -p "Log lines of history to read (default 50)>" LOGLINES
			if [ -z "$LOGLINES" ]; then
				LOGLINES=50
			fi
			if [ $LOGLINES -lt 10000 ] && [ $LOGLINES -gt 0 ] && [[ $LOGLINES =~ ^[0-9]+$ ]]; then
				echo
				echo $CONFIG | sed "s/\[MYIP\]/$MYIP/g" | sed "s/\[MYPORT\]/$MYPORT/g" | sed "s+\[LOGSPATH\]+$LOGSPATH+g" | sed "s/\[LOGLINES\]/$LOGLINES/g" > config.json &&
				(npm install && echo "Run: node index.js") ||
				(rm config.json && echo "Couldn't build the configuration..")
			else
				echo "Log history limited to 1-9999. Try again"
			fi
		else
			echo "Invalid port. Try again"
		fi
	else
		echo "Invalid IP address, try again"
	fi
else
	echo "Cannot find config_default.json, try redownloading the git repo"
fi