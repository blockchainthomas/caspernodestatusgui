## Casperlabs node status gui
----

Quick and dirty node status grabber gui for alpha testers

Easy deployment. Make sure you have **Node.js**

	$ git clone https://gitlab.com/blockchainthomas/caspernodestatusgui.git
	$ cd caspernodestatusgui
	$ ./build.sh
	$ node index.js

Browse (nodeip):8080

Default port is **8080** and the node relay defaults to **8089** so they should be opened on NAT router

	$ nano index.js

Change "var port = 8080" if needed.

Change "var Nodeip = '127.0.0.1'" or pass an ip on the cli if you want to scan another node.

Plans on allowing all nodes to be scanned and having details/filters by node displayed on front end.
