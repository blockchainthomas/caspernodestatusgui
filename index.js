/* ***

7-27-20
@blockchainthomas

*** */

const express = require('express');
const http = require('http');
const path = require('path');
const fs = require('fs');
const Async = require('async');
const app = express();
const Tail = require('nodejs-tail');
const { exec } = require('child_process');
const url = require('url');

console.log(__dirname);

var configjson = null;
var validator_IPs = {};
var emptyjson = null;
var procs_count = 0;
var peers_object = {};
var myIp = '';

console.log('Loading: config.json');

try {
	let configfile = fs.readFileSync('config.json');
	configjson = JSON.parse(configfile);
	myIp = configjson.myIp;
} catch(err) {
	console.log(err);
	console.log('Could not load "config.json - Please try running build.sh"');
	console.log('Exiting');
	process.exit(1);
}

console.log('Creating validator list...');

try {
	let sample = fs.readFileSync('assets/json/json-empty.json');
	emptyjson = JSON.parse(sample);
} catch(err) {
	console.log(err);
	console.log('Failed to load json-empty.json');
	console.log('Exiting');
	process.exit(1);
}

try {
	validator_IPs = {
		"validators": []
	};

	let dashed = myIp.split('.').join('-');
	fs.writeFileSync('assets/json/'+dashed+'.json', JSON.stringify(emptyjson));
	validator_IPs.validators.push(myIp);
	fs.writeFileSync('validators.json', JSON.stringify(validator_IPs));

	// add other validator GET functions later, here

	Object.entries(validator_IPs.validators).forEach(([key, value], index) => {
		global['doGets'+index] = function() {
			getData(value).then(function(gets_data) {
				let dashed = value.split('.').join('-');
				gets_data = JSON.parse(gets_data);
				fs.writeFileSync('assets/json/'+dashed+'.json', JSON.stringify(gets_data));
			})
			.catch(function(err) {
				// console.log(err);
				console.log(" -Could not connect to "+value);
			});
		}
	});

	setTimeout(function() {
		Object.entries(validator_IPs.validators).forEach(([key, value], index) => {
			global['doGets'+index]();
		});

		setInterval(function() {
			Object.entries(validator_IPs.validators).forEach(([key, value], index) => {
				global['doGets'+index]();
			});
		},30000);
	},3000);
} catch(err) {
	console.log(err);
	console.log('Failed to create validators.json');
	console.log('Exiting');
	process.exit(1);
}

var port = configjson.port ? configjson.port : 8080;
var relayPort = configjson.relayPort ? configjson.relayPort : 8089;
var loglines = configjson.loglines ? configjson.loglines : 50;
var logspath = configjson.logspath ? configjson.logspath : null;
var logscontent = [];

if(logspath) {
	console.log('Logging enabled from '+logspath);
	const tail = new Tail(logspath);

	tail.on('line', (line) => {
		try {
			let obj = JSON.parse(line);
			if(logscontent.length > loglines) logscontent.splice(0,1);
			logscontent.push(obj);
		} catch(err) {
			console.log(' -Error parsing logs');
		}
	});

	tail.watch();
} else {
	console.log('Logging disabled');
}

function start_proc(command, callback) {
	if(procs_count > 10) {
		return false;
	}

	procs_count += 1;
	var output = {
		success: "",
		error: "",
		code: 0
	};

	var proc = exec(command);

	proc.stdout.on('data', function(stdout) {
		output.success += stdout;
	});

	proc.stderr.on('data', function(stderr) {
		output.error += stderr;
	});

	proc.on('exit', function(code) {
		output.code = code;
		procs_count -= 1;
		if(typeof callback == 'function') callback(output);
	});
}

function getRoutes(folder) {
	fs.readdirSync(folder).forEach(function(file) {
		var name = path.join(folder, file);
		var stat = fs.lstatSync(name);

		if(stat.isDirectory()) {
			getRoutes(name);
		} else {
			console.log('Loading: '+name);
			app.get('/'+name, function(req, res) {
				res.sendFile(path.join(__dirname+'/'+name));
			});
		}
	});
}

function getData(ip) {
	return new Promise(function(resolve, reject) {
		var request = http.get('http://'+ip+':40403/status', function(resp) {
			var chunks = '';
			// console.log(' +getting: '+ip);

			resp.on('data', function(data) {
				chunks += data;
			});

			resp.on('end', function() {
				resolve(chunks.toString());
			});
		})
		.on('error', function(err) {
			console.log(err);
			reject('{"error":"Cannot connect to node endpoint '+ip+':40403"}');
		});

		request.setTimeout(5000, function() {
			console.log(' -timeout, aborting');
			request.abort();
		});
	});
}


app.get('/', function(req, res) {
	res.sendFile(path.join(__dirname+'/index.html'));
});

app.get('/favicon.ico', function(req, res) {
	res.sendFile(path.join(__dirname+'/favicon.ico'));
});

app.get('/config.json', function(req, res) {
	res.sendFile(path.join(__dirname+'/config.json'));
});

app.get('/validators.json', function(req, res) {
	res.sendFile(path.join(__dirname+'/validators.json'));
});

getRoutes('assets');
console.log('');

try {
	console.log('Starting GUI server on 127.0.0.1:'+port);
	app.listen(port);

	console.log('Starting proxy to validators:40403/status on port '+relayPort);
	var server = http.createServer(function(req, res) {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Headers', '*');
		res.writeHead(200);

		var response = {
			"validators":[]
		};

		for(var i = 0; i < validator_IPs.validators.length; i++) {
			try {
				let dashed = validator_IPs.validators[i].split('.').join('-');
				let data = fs.readFileSync('assets/json/'+dashed+'.json');
				data = JSON.parse(data);
				data.ip = validator_IPs.validators[i];
				response.validators.push(data);
			} catch(err) {
				console.log(err);
				console.log(" -problem loading data from "+validator_IPs.validators[i]+". Keeping old data");
			}
		}

		response.logs = logscontent;
		res.end(JSON.stringify(response));

	}).listen(relayPort);
} catch(failed) {
	console.log("Cannot start server");
	console.log(failed);
	process.exit(1);
}
